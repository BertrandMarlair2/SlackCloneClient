const drawerWidth = 270

const DashboardStyle = () => {
    return ({
        drawerOpen: {
            marginLeft: drawerWidth,
            padding: 10,
        },
        drawerClose: {
            marginLeft: 95,
            padding: 10,
        },
    })
}

export default DashboardStyle