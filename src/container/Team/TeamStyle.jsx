const TeamStyle = {
    container: {
        maxWidth: 450,
        margin: 'auto',
        paddingTop: '20vh',
    },
    form: {
        padding: 20
    },
    submit: {
        marginTop: 20
    }
}

export default TeamStyle